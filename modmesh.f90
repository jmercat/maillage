module modmesh
  implicit none
  !nombre max de points et de triangles
  integer,parameter::npmax = 1e6
  integer,parameter::nedmax = 5e5
  integer,parameter::ntmax = 6e6
  integer,parameter::nqmax = 6e5

  integer,dimension(0:2,1:2),parameter::iare=reshape((/1,2,0,2,0,1/),(/3,2/)) !(1,2),(2,0),(0,1)
  !type Point
  type Point
     integer::num                    !numero du point
     real(kind=8),dimension(3)::coor !coordonnee du point ; nb : en 2D la 3e coord est nulle
     integer::ref                    !tag associe au point
     logical::exist                  !false si le point n'est pas present dans le maillage
     integer::nxt,tmp
  end type Point
  !type Arete : sert a priori uniquement pour les aretes de frontieres
  type Edge
     integer::num               !numero de l'arete
     integer,dimension(0:1)::v    !indice des 2 sommets composant l'arete
     integer::numT              !1 tr contenant cet arete (numT = 3*k+i)
     integer::ref               !tag associe a l'arete
     logical::infront           !vrai si l'arete appartient au front, faux sinon
     logical::exist            !false si l'arete n'est pas presente dans le maillage
     integer::nxt,tmp
  end type Edge
  !type Triangle
  type Triangle
     integer::num               !numero du triangle
     integer,dimension(0:2)::v    !indice des 3 sommets composant le triangle
     integer,dimension(0:2)::ve   !indice des 3 aretes composant le triangle
     integer::ref               !tag associe au triangle
     integer,dimension(0:2)::adja !tab des voisins du triangles
     logical::exist            !false si le tr n'est pas present dans le maillage
     integer::nxt
  end type Triangle
  !type Quadrangle
  type Quadrangle
     integer::num               !numero du quad
     integer,dimension(0:3)::v    !indice des 4 sommets 
     integer,dimension(0:3)::ve   !indice des 4 aretes 
     integer::ref               !tag associe au quad
     integer,dimension(0:3)::adja !tab des voisins
     logical::exist            !false si le quad n'est pas present dans le maillage
     integer::nxt
  end type Quadrangle
  !type Mesh
  type Mesh
     integer::np,ned,nt ,nq                      !nb de point, d'arete, de triangle, de quad
     integer::npnxt,nednxt,ntnxt,nqnxt    
     type(Point),dimension(npmax)::mpoint
     type(Triangle),dimension(ntmax)::mtriangle
     type(Quadrangle),dimension(nqmax)::mquad
     type(Edge),dimension(nedmax)::medge
  end type Mesh

contains
  subroutine readmesh(maill,filename)
    implicit none
    type(Mesh),intent(inout)::maill
    character(len=*),intent(in)::filename
    character(len=50)::ch
    real(kind=8)::dtmp
    integer::ver,i


    open(unit=1,file=filename,status="old")
    !
    read(1,*) ch,ver
    !print*,"on a lu",trim(ch)
    !print*,ver,trim(ch)=="MeshVersionFormatted"

    do while(trim(ch)/="End")
       select case(trim(ch))
       case ("MeshVersionFormatted")
          print*,"VERSION READING"
       case ("Dimension")
          print*,"DIMENSION READING"
       case("Vertices")
          read(1,*) maill%np
          print*,"NUMBER OF NODES : ",maill%np
          do i=1,maill%np
             maill%mpoint(i)%num = i
             read(1,*) maill%mpoint(i)%coor,dtmp
             maill%mpoint(i)%ref = int(dtmp)
             maill%mpoint(i)%exist=.true.
             maill%mpoint(i)%nxt=0
          end do
       case("Triangles")
          read(1,*) maill%nt
          print*,"NUMBER OF TRIANGLES : ",maill%nt
          do i=1,maill%nt
             read(1,*) maill%mtriangle(i)%v,maill%mtriangle(i)%ref
             maill%mtriangle(i)%exist=.true.
             maill%mtriangle(i)%nxt=0
          end do
       case("Edges")
          read(1,*) maill%ned
          print*,"NUMBER OF EDGES : ",maill%ned
          do i=1,maill%ned
             read(1,*) maill%medge(i)%v,maill%medge(i)%ref
             maill%medge(i)%exist = .true.
             maill%medge(i)%nxt = 0
          end do
      case("Quadrilaterals")
          read(1,*) maill%nq
          print*,"NUMBER OF QUADS : ",maill%nq
          do i=1,maill%nq
             read(1,*) maill%mquad(i)%v,maill%mquad(i)%ref
             maill%mquad(i)%exist=.true.
             maill%mquad(i)%nxt=0
          end do
  
       end select
       read(1,*) ch
       !print*,"on a lu ",trim(ch)
    end do
    maill%npnxt = maill%np + 1
    do i=maill%npnxt,npmax-2
       maill%mpoint(i)%nxt= i+1
    end do
    maill%ntnxt = maill%nt + 1
    do i=maill%ntnxt,ntmax-2
       maill%mtriangle(i)%nxt= i+1
    end do
    maill%nqnxt = maill%nq + 1
    do i=maill%nqnxt,nqmax-2
       maill%mquad(i)%nxt= i+1
    end do
    maill%nednxt = maill%ned + 1
    do i=maill%nednxt,nedmax-2
       maill%medge(i)%nxt= i+1
    end do

    close(1)
  end subroutine readmesh

  subroutine savemesh(maill,filename)
    implicit none
    type(Mesh),intent(in)::maill
    type(Point),dimension(npmax)::vertex
    type(Triangle),dimension(ntmax)::tr
    type(Quadrangle),dimension(nqmax)::quad
    type(Edge),dimension(nedmax)::ed
    character(len=*),intent(in)::filename
    character(len=50)::ch
    real(kind=8)::dtmp
    integer::ver,i,ned,np,nt,nq

    vertex = maill%mpoint
    tr     = maill%mtriangle
    quad   = maill%mquad
    ed     = maill%medge


    open(unit=1,file=filename,status="unknown",form='formatted')

    write(1,*) "MeshVersionFormatted 2"
    write(1,*) "Dimension 3"
    write(1,*) " "
    write(1,*) "Vertices"
    !comptage des points existant
    np = 0
    do i=1,maill%np
       if(vertex(i)%exist) then
          np=np+1
          vertex(i)%tmp=np
       end if
    end do
    write(1,'(I10)') np
    do i=1,maill%np
       if(vertex(i)%exist) then 
          write(1,'(f19.12,2X,f19.12,2X,f19.12,2X,I3)') vertex(i)%coor,vertex(i)%ref
       end if
    end do
    if(maill%ned/=0) then
       write(1,*) "Edges"
       !comptage des aretes existantes
       ned = 0
       do i=1,maill%ned
          if(ed(i)%exist) then
             ned=ned+1
             ed(i)%tmp = ned
          end if
       end do

       write(1,'(I10)') ned
       do i=1,maill%ned
          if(ed(i)%exist) write(1,*) ed(i)%v,ed(i)%ref
       end do
    end if
    if(maill%nt/=0) then
       write(1,*) "Triangles"
       !comptage des triangles existants
       nt = 0
       do i=1,maill%nt
          if(tr(i)%exist) nt=nt+1
       end do
       write(1,'(I10)') nt
       do i=1,maill%nt
          if(tr(i)%exist) write(1,*) vertex(tr(i)%v(0))%tmp,&
               vertex(tr(i)%v(1))%tmp,vertex(tr(i)%v(2))%tmp,tr(i)%ref
       end do
    end if
    if(maill%nq/=0) then
       write(1,*) "Quadrilaterals"
       !comptage des quads existants
       nq = 0
       do i=1,maill%nq
          if(quad(i)%exist) nq=nq+1
       end do
       write(1,'(I10)') nq
       do i=1,maill%nq
          if(quad(i)%exist) write(1,*) vertex(quad(i)%v(0))%tmp,&
               vertex(quad(i)%v(1))%tmp,vertex(quad(i)%v(2))%tmp,vertex(quad(i)%v(3))%tmp,quad(i)%ref
       end do
    end if
    write(1,*) "End"
  end subroutine savemesh
end module modmesh
