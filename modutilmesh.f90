module modutil
  use modmesh
  implicit none
contains
  function ajoutpoint(M,coor) result(n)
    type(Mesh),intent(inout)::M
    real(kind=8),dimension(3),intent(in)::coor
    integer::n

    if(M%npnxt==0) then
       print*,"impossible ajouter nouveau point",M%npnxt
       stop
    end if
    n = M%npnxt
    if(M%npnxt>M%np) M%np=M%npnxt
    M%npnxt = M%mpoint(n)%nxt
    M%mpoint(n) = point(n,coor,1,.true.,0,0)

  end function ajoutpoint
  subroutine delpoint(M,num) 
    type(Mesh),intent(inout)::M
    integer,intent(in)::num
    integer::n,i
    
    if(.not.m%mpoint(num)%exist) then
       print*,"pbs point inexistant",num
       stop
    end if
    m%mpoint(num)%exist = .false.
    m%mpoint(num)%nxt = M%npnxt
    M%npnxt = num
    if(num == npmax ) then
       m%np = m%np-1
    end if
  end subroutine delpoint
  function ajoutedge(M,num,numT) result(n)
    type(Mesh),intent(inout)::M
    integer,dimension(2),intent(in)::num
    integer,intent(in)::numT
    integer::n

    if(M%nednxt==0) then
       print*,"impossible ajouter nouvelle edge",M%nednxt,M%ned
       stop
    end if
    n = M%nednxt
    if(M%nednxt > M%ned) M%ned = M%ned+1
    M%nednxt = M%medge(n)%nxt
    M%medge(n) = edge(n,num,numT,1,.false.,.true.,0,0)
  end function ajoutedge
  subroutine deledge(M,num) 
    type(Mesh),intent(inout)::M
    integer,intent(in)::num
    integer::i
    if(.not.m%medge(num)%exist) then
       print*,"edge inexistante",num
       stop
    end if
    m%medge(num)%exist = .false.
    m%medge(num)%infront = .false.
    m%medge(num)%nxt = M%nednxt
    M%nednxt = num
    if(num == m%ned ) then
       m%ned = m%ned-1
    end if
  end subroutine deledge
  function ajouttriangle(M,nump,numed) result(n)
    type(Mesh),intent(inout)::M
    integer,dimension(3),intent(in)::nump,numed
    integer::n

    if(M%ntnxt == 0 ) then
       print*,"impossible ajouter nouveau tr"
       stop
    end if
    n = M%ntnxt
    if(M%ntnxt > M%nt) M%nt = M%nt+1
    M%ntnxt = M%mtriangle(n)%nxt
    M%mtriangle(n) = triangle(M%nt,nump,numed,1,0,.true.,0)

  end function ajouttriangle
  subroutine deltriangle(M,num)
    type(Mesh),intent(inout)::M
    integer,intent(in)::num
    if(.not.m%mtriangle(num)%exist) then
       print*,"tr inexistant",num
       stop
    end if
    m%mtriangle(num)%nxt = m%ntnxt
    m%mtriangle(num)%v = 0
    m%ntnxt = num
    if(num==m%nt) m%nt = m%nt-1
  end subroutine deltriangle
  function ajoutquad(M,nump) result(n)
    type(Mesh),intent(inout)::M
    integer,dimension(:),intent(in)::nump
    integer::n

    if(M%nqnxt == 0 ) then
       print*,"impossible ajouter nouveau quad"
       stop
    end if
    n = M%nqnxt
    if(M%nqnxt > M%nq) M%nq = M%nq+1
    M%nqnxt = M%mquad(n)%nxt
    M%mquad(n) = quadrangle(M%nq,nump,0,1,0,.true.,0)

  end function ajoutquad
  subroutine delquad(M,num)
    type(Mesh),intent(inout)::M
    integer,intent(in)::num
    if(.not.m%mquad(num)%exist) then
       print*,"quad inexistant",num
       stop
    end if
    m%mquad(num)%nxt = m%nqnxt
    m%mquad(num)%v = 0
    m%nqnxt = num
    if(num==m%nq) m%nq = m%nq-1
  end subroutine delquad
  function airetr(c1,c2,c3) 
    real(kind=8),dimension(3),intent(in)::c1,c2,c3
    real(kind=8)::airetr

    airetr = (c2(1)-c1(1))*(c3(2)-c1(2)) - (c2(2)-c1(2))*(c3(1)-c1(1))
  end function airetr
  
  subroutine initmesh(maill)
    type(Mesh),intent(inout)::maill
    integer::i
    maill%np = 0
    maill%nt = 0
    maill%nq = 0
    maill%ned = 0

    maill%npnxt = maill%np + 1
    do i=maill%npnxt,npmax-2
       maill%mpoint(i)%nxt= i+1
    end do
    maill%ntnxt = maill%nt + 1
    do i=maill%ntnxt,ntmax-2
       maill%mtriangle(i)%nxt= i+1
    end do
    maill%nqnxt = maill%nq + 1
    do i=maill%nqnxt,nqmax-2
       maill%mquad(i)%nxt= i+1
    end do
    maill%nednxt = maill%ned + 1
    do i=maill%nednxt,nedmax-2
       maill%medge(i)%nxt= i+1
    end do
  end subroutine initmesh
end module modutil
