#
# Makefile fortran 90
#

# Nom du compilateur
FC=gfortran

# Liste des objets
OBJS= modutilmesh.o optimisation.o main.o modmesh.o  modfront.o 

prog: $(OBJS)
	$(FC) $(OBJS) -o $@

%.o: %.f90
	$(FC) -c -O $<

main.o : modmesh.o  modfront.o
modfront.o : modmesh.o
modutilmesh.o : modmesh.o

clean:
	rm -f *.o *~ *.mod

# Rajouter les regles de dependance entre modules. Par exemple, si main.f90 contient une instruction
# "use addition" et que le module "addition" est defini dans addition.f90:
# main.o : addition.o
#
# On peut aussi rajouter des dependances du type "include"
# main.o : fichier_include.h

