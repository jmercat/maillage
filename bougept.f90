program bougept
implicit none
!include "mpif.h"

	!integer,dimension(MPI_STATUS_SIZE)::Statu
	integer::Np,Me,Him, statinfo
	
	integer::im,jm,pcav1, pcav2
	real*8,dimension(:,:), allocatable:: Ptf, Ptm, Vptm, Aptm ! listes des coordonnées des points fixes et mobiles
	integer:: NPtf, NPtm, NPtb, NPtx !nombre de points fixes et mobiles et cond aux bords
	real*8::ef,e,nu,k,eb,m, x(2,2), centre(2), rayon !charge bords, charge, coef viscosité, coef coulomb, charge bord, masse 
	integer::degb, degp ! puissance affectant les forces de distance du bord et des particules
	real*8::dt,Tfinal,Ts,T,d1,d2,Lx, Ly, ptb(2), cote, diviseur, maxd
	T=0
	Tfinal=30000
	dt=0.1
	
	
	nu=0.1
	k=0.1
	!eb=100*e
	m=1
	degb = 2
	degp = 3
	diviseur = 1.0005
	
	call system("./grilleInit.exe")

	open(unit=1,file="ptfixes.txt",status="old",form='formatted')
	open(unit=2,file="grilleHex.dat",status="unknown",form='formatted')
	open(unit=3,file="jean_mercat.txt",status="old",form='formatted')
	
	read(3,*) x(1,1),x(1,2)
	read(3,*) x(2,1),x(2,2)
	read(3,*) Nptx
	
	read(1,*) NPtf
	!NPtf =60
	NPtb = 20
	read(2,*) NPtm
	print*,NPtm
	
	e=0.2/sqrt(real(NPtm))
	ef = 0.2/sqrt(real(NPtm))
	
	Lx = x(2,1)-x(1,1)
	Ly = x(2,2)-x(1,2)
	cote = Lx/Nptx
	print*,"cote" ,cote
	!x(1,1)=x(1,1)-0.01*Lx
	!x(1,2)=x(1,2)-0.01*Ly
	
	!x(2,1)=x(2,1)+0.01*Lx
	!x(2,2)=x(2,2)+0.01*Ly

	!x(1,1)=sqrt(real(Nptm))/Lx*x(1,1)-0.1
	!x(1,2)=sqrt(real(Nptm))/Ly*x(1,2)-0.1
	
	!x(2,1)=sqrt(real(Nptm))/Lx*x(2,1)+0.1
	!x(2,2)=sqrt(real(Nptm))/Ly*x(2,2)+0.1
	
	!centre = (x(2,:)-X(1,:))/2
	!rayon = max(real(Lx)/sqrt(real(Nptm)),real(Ly)/sqrt(real(Nptm)))
	
	!print*, rayon
	
	!print*, x(2,1)*Lx/sqrt(real(Nptm))
	
	print*, Lx, Ly
	
	allocate(Ptf(2,NPtf),Ptm(2,NPtm), Vptm(2,Nptm), Aptm(2,NPtm))
	
	Aptm=0
	Vptm=0
	
	do im=1,NPtf
		read(1,*) Ptf(1,im), Ptf(2,im)
		!Ptf(1,i)= Ptf(1,i)*sqrt(real(Nptm))/Lx
		!Ptf(2,i)= Ptf(2,i)*sqrt(real(Nptm))/Ly
	end do
	
	do im=1,NPtm
		read(2,*) Ptm(1,im), Ptm(2,im)
		!Ptm(1,i)= Ptm(1,i)*sqrt(real(Nptm))/Lx
		!Ptm(2,i)= Ptm(2,i)*sqrt(real(Nptm))/Ly
	end do
	
	close(1)
	close(2)
	close(3)
	maxd=cote
	do while(maxd> 0.01*cote)
		dt=dt/diviseur
		!print*, maxd
		maxd=0
		!print*,dt
		call acceleration
		!print*, Aptm
		do im=1,Nptm
			Vptm(:,im)=Vptm(:,im)/2+dt*Aptm(:,im)
			!Vptm(:,i)=dt*Aptm(:,i)
			!Ptm(:,i)=Ptm(:,i)+min(dt,0.5*cote/norme(Vptm(:,i)))*Vptm(:,i)
			Ptm(:,im)=Ptm(:,im)+dt*Vptm(:,im)
			maxd = max(maxd,norme(dt*Vptm(:,im)))
		end do
		call replacePt
	!	T=T+dt
	!	pcav1 = floor(100*T/(Tfinal))
	!	if (pcav1==pcav2) then
	!		print*,pcav1, "%"
	!		pcav2 = pcav1+1
			
		!	call system("")
	!	end if
	
	end do
			
	open(unit=2,file="ptmobiles.txt",status="unknown",form='formatted')
			do im=1,NPtm
				!write(2,'(2f19.12)') Ptm(1,i)*real(Lx)/sqrt(real(Nptm)), Ptm(2,i)*real(Ly)/sqrt(real(Nptm))
				write(2,'(2f19.12)') Ptm(1,im), Ptm(2,im)
			end do
			close(2)
			CALL System("gnuplot script")
	
	contains
	subroutine acceleration()
		implicit none
		real*8,dimension(2)::vtemp
		integer::ii,jj
		Aptm=0
		do ii = 1, Nptm
			!print*, "acccel", ii, jj, T
			do jj= ii+1,Nptm
					Aptm(:,ii) = Aptm(:,ii) + distance(ii,jj)
					Aptm(:,jj) = Aptm(:,jj) - distance(ii,jj)
			end do
			do jj=1,Nptf
				Aptm(:,ii) = Aptm(:,ii) + distancef(ii,jj)
				if (norme(Ptm(:,ii)-Ptf(:,jj))==0 .and. T/=Tfinal) then
					Ptm(1,ii)=Ptm(1,ii)+cote*1e-4
					print*, "acccel", ii, jj, T
					T=Tfinal
				end if
			end do
			!viscosité
		!	vtemp=Vptm(:,ii)+dt*Aptm(:,ii)
		!	Aptm(:,ii) = Aptm(:,ii) - nu*vtemp*norme(vtemp)
			
		end do
	end subroutine
	
	subroutine replacePt()
		implicit none
		real*8::Ptmprev(2)
		integer:: ii
	!	do i = 1, Nptm
	!		print*, norme(Ptm(:,i))
	!		if (norme(Ptm(:,i)-centre)>rayon) then
	!			ptm(:,i) = centre + rayon*Ptm(:,i)/norme(Ptm(:,i))
	!		end if
	!	end do
		
		do ii = 1, Nptm
		!	print*,Ptm(:,i)
		
				if (isnan(Ptm(1,ii)) .and. T/=Tfinal) then
					print*, "avant", ii
					T=Tfinal
					return
				end if
				Ptmprev = Ptm(:,ii)
				!Ptm(1,i)=modulo(Ptm(1,i)-x(1,1),Lx)+x(1,1)
				!Ptm(2,i)=modulo(Ptm(2,i)-x(1,2),Ly)+x(1,2)
			
				if (mod(Ptm(1,ii)-x(1,1),Lx)>0) then
					Ptm(1,ii)=mod(Ptm(1,ii)-x(1,1),Lx)+x(1,1)
				else
					Ptm(1,ii)=mod(Ptm(1,ii)-x(1,1),Lx)+x(2,1)
				endif
			
				if (mod(Ptm(2,ii)-x(1,2),Ly)>0) then
					Ptm(2,ii)=mod(Ptm(2,ii)-x(1,2),Ly)+x(1,2)
				else
					Ptm(2,ii)=mod(Ptm(2,ii)-x(1,2),Ly)+x(2,2)
				endif
			
			
			
			if (isnan(Ptm(1,ii)) .and. T/=Tfinal) then
				print*, "après", Ptm(:,ii), Ptmprev(:), T
				T=Tfinal
				return
			end if
			
		!	print*,Ptm(:,i)
		end do
		
		
!		do i = 1, Nptm
!			Ptm(1,i)=max(Ptm(1,i),x(1,1)+0.1)
!			Ptm(1,i)=min(Ptm(1,i),x(2,1)-0.1)
!			Ptm(2,i)=max(Ptm(2,i),x(1,2)+0.1)
!			Ptm(2,i)=min(Ptm(2,i),x(2,2)-0.1)
!		end do
		
	end subroutine
	
	function norme(v)
		implicit none
		real*8::norme,v(2)
		
		norme = sqrt(v(1)**2+v(2)**2)
		
	end function
	
	function distance(i,j)
		implicit none
		integer,intent(in)::i,j
		real*8,dimension(2)::distance
		real*8::cteta, dtot,tetadiag
		
		if (norme(Ptm(:,i)-Ptm(:,j))>1e-6 ) then
			tetadiag = atan(Ly/Lx)
			cteta = abs(Ptm(1,i)-Ptm(1,j))/norme(Ptm(:,i)-Ptm(:,j))
			
			if (acos(cteta)>tetadiag) then
				dtot = Ly/sqrt(1-cteta)
			else
				dtot = Lx/cteta
			end if
		
			if (dtot-norme(Ptm(:,i)-Ptm(:,j))>1e-6) then
				
				distance=(Ptm(:,i)-Ptm(:,j))*(((e*e*k)/m)/norme(Ptm(:,i)-Ptm(:,j))**degp)
				
				!if (abs(distance(1))>10000 .and. T/=Tfinal) then
				!	print*, "NAAAAAAN1", norme(Ptm(:,i)-Ptm(:,j)), distance(1)
					!T=Tfinal
				!end if
				
				
				distance=distance-(Ptm(:,i)-Ptm(:,j))*(((e*e*k)/m)/((dtot-norme(Ptm(:,i)-Ptm(:,j)))**(degp-1)*norme(Ptm(:,i)-Ptm(:,j))))
				
				!if (abs(distance(1))>10000 .and. T/=Tfinal) then
				!	print*, "NAAAAAAN1", norme(Ptm(:,i)-Ptm(:,j)), distance(1)
				!	T=Tfinal
				!end if
				
			!	if (isnan(distance(1))) then
			!		print*, "NAAAAAAN2"
			!		T=Tfinal
			!	end if
			else
				Ptm(1,i)=Ptm(1,i)+1e-3*cote
				Ptm(2,i)=Ptm(2,i)+1e-3*cote
				call replacePt
				distance=(Ptm(:,i)-Ptm(:,j))*(((e*e*k)/m)/norme(Ptm(:,i)-Ptm(:,j))**degp)
				distance=distance-(Ptm(:,i)-Ptm(:,j))*(((e*e*k)/m)/((dtot-norme(Ptm(:,i)-Ptm(:,j)))**(degp-1)*norme(Ptm(:,i)-Ptm(:,j))))
				print*, "bof bof 1"
			end if
		end if
	end function
	
	
	function distancef(i,j)
		implicit none
		integer,intent(in)::i,j
		real*8,dimension(2)::distancef
		real*8::cteta, dtot,tetadiag
		
		
		
		
		if (norme(Ptm(:,i)-Ptf(:,j))>1e-6) then
		
			tetadiag = atan(Ly/Lx)
			cteta = abs(Ptm(1,i)-Ptf(1,j))/norme(Ptm(:,i)-Ptf(:,j))
		
			if (acos(cteta)>tetadiag) then
				dtot = Ly/sqrt(1-cteta)
			else
				dtot = Lx/cteta
			end if
		
			if(dtot-norme(Ptm(:,i)-Ptf(:,j))>1e-6) then
			distancef=(Ptm(:,i)-Ptf(:,j))*(((e*ef*k)/m)/norme(Ptm(:,i)-Ptf(:,j))**degp)
			
			!if (abs(distance(1))>10000 .and. T/=Tfinal) then
			!	print*, "NAAAAAAN1", norme(Ptm(:,i)-Ptm(:,j)), distance(1)
				!T=Tfinal
			!end if
			
			!distancef=distancef-(Ptm(:,i)-Ptf(:,j))*(((e*ef*k)/m)/((dtot-norme(Ptm(:,i)-Ptf(:,j)))**(degp-1)*norme(Ptm(:,i)-Ptf(:,j))))
			
			!if (abs(distance(1))>10000 .and. T/=Tfinal) then
			!	print*, "NAAAAAAN1", norme(Ptm(:,i)-Ptm(:,j)), distance(1)
			!	T=Tfinal
			!end if
			
		!	if (isnan(distance(1))) then
		!		print*, "NAAAAAAN2"
		!		T=Tfinal
		!	end if
			else
				Ptm(1,i)=Ptm(1,i)+1e-3*cote
				Ptm(2,i)=Ptm(2,i)+1e-3*cote
				call replacePt
				distancef=(Ptm(:,i)-Ptf(:,j))*(((e*e*k)/m)/norme(Ptm(:,i)-Ptf(:,j))**degp)
				distancef=distancef-(Ptm(:,i)-Ptf(:,j))*(((e*e*k)/m)/((dtot-norme(Ptm(:,i)-Ptf(:,j)))**(degp-1)*norme(Ptm(:,i)-Ptf(:,j))))
			!	print*, "bof bof 2", cteta, abs(Ptm(1,i)-Ptf(1,j)),norme(Ptm(:,i)-Ptf(:,j))
			end if
		end if
	end function

end program
