module modfront
  use modmesh
  implicit none
  type front
     integer::size                   !indice du dernier element dans le tab list
     integer::curc                   !indice du premier element non traite dans list
     integer,dimension(nedmax)::list !tableau contenant des indices d'aretes
  end type front
  
contains
  !ajout de num au front fr
  subroutine ajoutfront(fr,num)
    type(front),intent(inout)::fr
    integer,intent(in)::num

    if(fr%size==nedmax) then
       print*,'impossible de rajouter un element au front'
       stop
    end if
    fr%size = fr%size+1
    fr%list(fr%size) = num
  end subroutine ajoutfront

end module modfront
