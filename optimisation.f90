 !Optimisation

module Optim
	use modmesh
	implicit none
  real*8,parameter::anglemax=100

contains

	subroutine test_angle(T1,T2,maillage,compteur)
	implicit none 
		type(mesh)::maillage
		integer,intent(in)::T1,T2
		integer,dimension(2)::PtAdj
		integer::p1,p2,n1,n2,n3,compteur
		integer::bp1,bp2,bn1,bn2,bn3
		real*8::angle,x1,y1,x2,y2,x3,y3,cocos
		real*8::bangle,bx1,by1,bx2,by2,bx3,by3,bcocos
		call PtAdja(T1,T2,PtAdj,maillage)
		if (PtAdj(1) <= 2) then
			if (PtAdj(1)==0) then
				p1=1
				p2=2
			else if (PtAdj(1)==1) then
				p1=0
				p2=2
			else
				p1=0
				p2=1
			end if	
			 n1=maillage%mtriangle(T1)%v(PtAdj(1))
			 n2=maillage%mtriangle(T1)%v(p1)
			 n3=maillage%mtriangle(T1)%v(p2)
			 
			 x1=maillage%mpoint(n1)%coor(1)
			 y1=maillage%mpoint(n1)%coor(2)
			 
			 x2=maillage%mpoint(n2)%coor(1)
			 y2=maillage%mpoint(n2)%coor(2)
			 
			 x3=maillage%mpoint(n3)%coor(1)
			 y3=maillage%mpoint(n3)%coor(2)
			 
			 cocos=(x3-x1)*(x2-x1)+(y3-y1)*(y2-y1)/(sqrt(((x3-x1)**2+(y3-y1)**2)*((x2-x1)**2+(y2-y1)**2)))
			 angle=acos(cocos)
			 
			 if (PtAdj(2)==0) then
				bp1=1
				bp2=2
			else if (PtAdj(2)==1) then
				bp1=0
				bp2=2
			else
				bp1=0
				bp2=1
			end if	
			 bn1=maillage%mtriangle(T2)%v(PtAdj(2))
			 bn2=maillage%mtriangle(T2)%v(bp1)
			 bn3=maillage%mtriangle(T2)%v(bp2)

			 bx1=maillage%mpoint(bn1)%coor(1)
			 by1=maillage%mpoint(bn1)%coor(2)

			 bx2=maillage%mpoint(bn2)%coor(1)
			 by2=maillage%mpoint(bn2)%coor(2)

			 bx3=maillage%mpoint(bn3)%coor(1)
			 by3=maillage%mpoint(bn3)%coor(2)

			 bcocos=((bx3-bx1)*(bx2-bx1)+(by3-by1)*(by2-by1))/(sqrt(((bx3-bx1)**2+(by3-by1)**2)*((bx2-bx1)**2+(by2-by1)**2)))

			 bangle=acos(bcocos)
			 
			! print*,'Angle 1 vaut',angle*180./3.14,'Angle 2 vaut',bangle*180/3.14
			 if (angle+bangle > 2*anglemax/180.*3.14) then
				call RetournementArrete(T1,T2,maillage)
				compteur=compteur+1
				!print*,'Angle 1 vaut',angle*180./3.14,'Angle 2 vaut',bangle*180/3.14
				!print*,'retournement',T1+248,T2+248
			 end if

		end if

	end subroutine

subroutine test_note(T1,maillage,note)
	implicit none 
		type(mesh)::maillage
		integer,intent(in)::T1
		integer::n1,n2,n3
		real*8::x1,y1,x2,y2,x3,y3,long1,long2,long3,c
		real*8::note,ere
			
			 n1=maillage%mtriangle(T1)%v(0)
			 n2=maillage%mtriangle(T1)%v(1)
			 n3=maillage%mtriangle(T1)%v(2)
			 
			 x1=maillage%mpoint(n1)%coor(1)
			 y1=maillage%mpoint(n1)%coor(2)
			 
			 x2=maillage%mpoint(n2)%coor(1)
			 y2=maillage%mpoint(n2)%coor(2)
			 
			 x3=maillage%mpoint(n3)%coor(1)
			 y3=maillage%mpoint(n3)%coor(2)
			 
			 long1=(y1-y3)**2+(x1-x3)**2
			 long2=(y2-y1)**2+(x2-x1)**2
			 long3=(y3-y2)**2+(x3-x2)**2
			 c=max(long1,long3,long2)
		
			 ere=aire(maillage,T1)

			 note=(sqrt(3.)*c)/(ere*4)
	

	end subroutine

	subroutine RetournementArrete(T1,T2,maillage)
	implicit none
		type(mesh):: maillage
        integer::i, j, nedge
        integer,intent(in)::T1,T2
        integer,dimension(2)::PtAdj
        
        call PtAdja(T1,T2,PtAdj,maillage)
        
        !changement des triangles #############le tag n'est pas traité
        
        if (maillage%mtriangle(T1)%v(mod(PtAdj(1)+1,3))==maillage%mtriangle(T2)%v(mod(PtAdj(2)+1,3))) then
			
			!changement des triangles adjacents
			maillage%mtriangle(T1)%adja(PtAdj(1))=3*(maillage%mtriangle(T2)%adja(mod(PtAdj(2)+2,3))/3)+PtAdj(1)
			maillage%mtriangle(T1)%adja(mod(PtAdj(1)+1,3))=3*T2+mod(PtAdj(1)+1,3)
			
			maillage%mtriangle(T2)%adja(PtAdj(2))=3*(maillage%mtriangle(T1)%adja(mod(PtAdj(1)+1,3))/3)+PtAdj(2)
			maillage%mtriangle(T2)%adja(mod(PtAdj(2)+2,3))=3*T1+mod(PtAdj(2)+2,3)
			
			!changement des points associés aux triangles
			maillage%mtriangle(T1)%v(mod(PtAdj(1)+2,3))= maillage%mtriangle(T2)%v(PtAdj(2))
			maillage%mtriangle(T2)%v(mod(PtAdj(2)+1,3))= maillage%mtriangle(T1)%v(PtAdj(1))
			
			
		elseif (maillage%mtriangle(T1)%v(mod(PtAdj(1)+1,3))==maillage%mtriangle(T2)%v(mod(PtAdj(2)+2,3))) then
		
			!changement des triangles adjacents
			maillage%mtriangle(T1)%adja(PtAdj(1))=3*(maillage%mtriangle(T2)%adja(mod(PtAdj(2)+1,3))/3)+PtAdj(1)
			maillage%mtriangle(T1)%adja(mod(PtAdj(1)+1,3))=3*T2+mod(PtAdj(1)+1,3)
			
			maillage%mtriangle(T2)%adja(PtAdj(2))=3*(maillage%mtriangle(T1)%adja(mod(PtAdj(1)+1,3))/3)+PtAdj(2)
			maillage%mtriangle(T2)%adja(mod(PtAdj(2)+1,3))=3*T1+mod(PtAdj(2)+2,3)
			
			!changement des points associés aux triangles
			maillage%mtriangle(T1)%v(mod(PtAdj(1)+2,3))= maillage%mtriangle(T2)%v(PtAdj(2))
			maillage%mtriangle(T2)%v(mod(PtAdj(2)+2,3))= maillage%mtriangle(T1)%v(PtAdj(1))
			
		else
			print*, "Problème de retournement d'arrete : adjacence"
        end if
        
	
	end subroutine
	
	function aire(maillage,Ntriangle)
		implicit none
		type(mesh) :: maillage
		integer,intent(in)::Ntriangle
		integer::n1,n2,n3
        real*8,dimension(2)::x1,x2,x3,v1,v2
        real*8:: aire
		
		
			n1=maillage%mtriangle(Ntriangle)%v(0)
			n2=maillage%mtriangle(Ntriangle)%v(1)
			n3=maillage%mtriangle(Ntriangle)%v(2)
			
			x1=maillage%mpoint(n1)%coor(1:2)
			x2=maillage%mpoint(n2)%coor(1:2)
			x3=maillage%mpoint(n3)%coor(1:2)
			
			v1=x2-x1
			v2=x3-x1
			
			aire=abs(v1(1)*v2(2)-v1(2)*v2(1))/2.	
	end function
	
!	function comparaison(vn1,vn2)
!	implicit none
!	integer, dimension(0:2)::vn1,vn2
!	integer::i,j, comparaison, compteur
!	
!	do i=0,2
!		do j=0,2
!			if (vn1(i)==vn2(j)) then
!				compteur=compteur+1
!			else
!				
!			end if
!		end do
!	end do
!	
!	if (compteur/=2) then
!		comparaison = 
!	end if
!	
!	end function
	
	subroutine trouveAdja(maillage)
		implicit none
		type(mesh)::maillage
		integer::i,j
		integer,dimension(2)::PtAdj
		
		do i=1,maillage%nt
			do j=i+1,maillage%nt
				call PtAdja(i,j,PtAdj,maillage)
				if (PtAdj(1)/=3) then
					maillage%mtriangle(i)%adja(PtAdj(1))=3*j+PtAdj(2)
					maillage%mtriangle(j)%adja(PtAdj(2))=3*i+PtAdj(1)
				end if
			end do	
		end do
	end subroutine
	


! cette subroutine renvoit les deux numeros locaux de 0 à 2 des sommets
! opposés des triangles adjacents T1 et T2
! si T1 et T2 ne sont pas adjacents, les numeros locaux sont mis à 3
subroutine PtAdja(T1,T2,PtAdj,maillage)
	implicit none
	type(mesh) :: maillage
	integer, intent(in)::T1,T2
	integer::i,j, compteur, n1(0:2), n2(0:2)
	integer,dimension(2)::PtAdj, pts1, pts2
	n1(0)=maillage%mtriangle(T1)%v(0)
	n1(1)=maillage%mtriangle(T1)%v(1)
	n1(2)=maillage%mtriangle(T1)%v(2)
	
	n2(0)=maillage%mtriangle(T2)%v(0)
	n2(1)=maillage%mtriangle(T2)%v(1)
	n2(2)=maillage%mtriangle(T2)%v(2)
	
	compteur=0
	
	PtAdj(1)=3
	PtAdj(2)=3
	
	if (T1 /=T2 ) then 
		do i=0,2
			do j=0,2
				if (n1(i)==n2(j)) then
					compteur= compteur+1
					pts1(compteur)= i
					pts2(compteur)= j
				end if
			end do
		end do
		
		
		
		if (compteur==2) then
			do i=0,2
				if (i/=pts1(1) .and. i /=pts1(2)) then
					PtAdj(1)= i
				end if
				if (i/=pts2(1) .and. i /=pts2(2)) then
					PtAdj(2)= i
				end if
			end do
		end if
	end if
	
end subroutine
	
end module
