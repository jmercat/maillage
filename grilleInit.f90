program grilleInit
implicit none

	integer::i,j,Ny ,Nx
	real*8::cote, hauteur, Lx, Ly, x(2,2)

	open(unit=1,file="jean_mercat.txt",status="old",form='formatted')
	open(unit=2,file="grilleHex.dat",status="unknown",form='formatted')
	read(1,*) x(1,1),x(1,2)
	read(1,*) x(2,1),x(2,2)
	read(1,*) Nx

	Lx=x(2,1)-x(1,1)
	Ly=x(2,2)-x(1,2)
	print*, "largeur hauteur", Lx, Ly
	cote = Lx/Nx
	hauteur = sqrt(3.)/2.*cote
	print*, "cote triangle, hauteur triangle", cote, hauteur
	
	Ny = int(Ly/hauteur)
	write(2,*) Nx*Ny
	do i=1,Nx
		do j =1,Ny
			write(2,*) (i-mod(j,2)/2.)*cote+x(1,1), j*hauteur+x(1,2)
		end do
	end do

end program
