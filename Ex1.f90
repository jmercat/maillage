program com
implicit none
include "mpif.h"

integer::Np,Me,Him, statinfo,i
real,dimension(1000)::A
integer,dimension(MPI_STATUS_SIZE)::Statu
A=0


call MPI_init (statinfo)
call MPI_COMM_RANK(mpi_comm_world,Me,statinfo)
call MPI_COMM_SIZE(mpi_comm_world,Np,statinfo)


if (Me ==3)then
	do i=1,1000
	A(i)=i
	end do
	
	call mpi_send(A,5,mpi_real,6,1000,mpi_comm_world,statinfo)
endif

if (Me==6)then
	call mpi_recv(A,5,mpi_real,3,1000,mpi_comm_world,Statu,statinfo)
endif

print*,Me,A
call mpi_finalize(statinfo)
end program
