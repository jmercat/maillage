program bougept
implicit none
include "mpif.h"

	integer,dimension(MPI_STATUS_SIZE)::Statu
	integer::Np,Me,Him, statinfo
	
	integer::i,j,pcav1, pcav2
	real*8,dimension(:,:), allocatable:: Ptf, Ptm, Vptm, Aptm, Aptmpart ! listes des coordonnées des points fixes et mobiles
	integer:: NPtf, NPtm !nombre de points fixes et mobiles
	real*8::e,nu,k,eb,m, x(2,2), centre(2), rayon ! charge, coef viscosité, coef coulomb, charge bord, masse 
	integer::degb, degp ! puissance affectant les forces de distance du bord et des particules
	real*8::dt,Tfinal,Ts,T,d1,d2,Lx, Ly
	T=0
	Tfinal=1
	dt=1
	
	e=0.0001
	nu=0.1
	k=1000
	eb=0!10*e
	m=1
	degb = 2
	degp = 3
	
	
	open(unit=1,file="ptfixes.txt",status="old",form='formatted')
	open(unit=2,file="grilleHex.dat",status="unknown",form='formatted')
	open(unit=3,file="jean_mercat.txt",status="old",form='formatted')
	
	read(3,*) x(1,1),x(1,2)
	read(3,*) x(2,1),x(2,2)
	
	!read(1,*) NPtf
	NPtf =26
	read(2,*) NPtm
	
	Lx = x(2,1)-x(1,1)
	Ly = x(2,2)-x(1,2)
	
	
	x(1,1)=sqrt(real(Nptm))/Lx*x(1,1)-0.1
	x(1,2)=sqrt(real(Nptm))/Ly*x(1,2)-0.1
	
	x(2,1)=sqrt(real(Nptm))/Lx*x(2,1)+0.1
	x(2,2)=sqrt(real(Nptm))/Ly*x(2,2)+0.1
	
	centre = (x(2,:)-X(1,:))/2
	rayon = max(real(Lx)/sqrt(real(Nptm)),real(Ly)/sqrt(real(Nptm)))
	
	print*, rayon
	
	print*, x(2,1)*Lx/sqrt(real(Nptm))
	
	allocate(Ptf(2,NPtf),Ptm(2,NPtm), Vptm(2,Nptm), Aptm(2,NPtm))
	
	Aptm=0
	Vptm=0
	
	do i=1,NPtf
		read(1,*) Ptf(1,i), Ptf(2,i)
		Ptf(1,i)= Ptf(1,i)*sqrt(real(Nptm))/Lx
		Ptf(2,i)= Ptf(2,i)*sqrt(real(Nptm))/Ly
	end do
	
	do i=1,NPtm
		read(2,*) Ptm(1,i), Ptm(2,i)
		Ptm(1,i)= Ptm(1,i)*sqrt(real(Nptm))/Lx
		Ptm(2,i)= Ptm(2,i)*sqrt(real(Nptm))/Ly
	end do
	
	close(1)
	close(2)
	close(3)
	
		call MPI_init (statinfo)
		call MPI_COMM_RANK(mpi_comm_world,Me,statinfo)
		call MPI_COMM_SIZE(mpi_comm_world,Np,statinfo)
	
	do while(T<Tfinal)
		call acceleration
		!print*, Aptm
		Vptm=Vptm+dt*Aptm
		Ptm=Ptm+dt*Vptm
		call replacePt
		T=T+dt
		pcav1 = floor(100*T/(Tfinal))
		if (pcav1==pcav2) then
			print*,pcav1, "%"
			pcav2 = pcav1+1
			if (me==0) then
				open(unit=2,file="ptmobiles.txt",status="unknown",form='formatted')
				do i=1,NPtm
					write(2,'(2f19.12)') Ptm(1,i)*real(Lx)/sqrt(real(Nptm)), Ptm(2,i)*real(Ly)/sqrt(real(Nptm))
				end do
				close(2)
				CALL System("gnuplot script")
				!	call system("")
			end if
		end if
	end do
	
	call mpi_finalize(statinfo)
	
	
	contains
	subroutine acceleration()
		implicit none
		Aptm=0
		
		print*, Np
		
		if (me<Np-1) then
			allocate(Aptmpart(2,Nptm/Np))
			
			do i = 1, Nptm/Np
				do j= 1,Nptm
					if (j>i) then
						Aptmpart(:,i) = Aptmpart(:,i) + e*e*k/m*(Ptm(:,i+me*(Nptm/Np))-Ptm(:,j))/&
						&(sqrt((Ptm(1,i+me*(Nptm/Np))-Ptm(1,j))**2+(Ptm(2,i+me*(Nptm/Np))-Ptm(2,j))**2))**degp
						!Aptmpart(:,j) = Aptm(:,j) - e*e*k/m*(Ptm(:,i)-Ptm(:,j))/(sqrt((Ptm(1,i)-Ptm(1,j))**2+(Ptm(2,i)-Ptm(2,j))**2))**degp
						!print*,sqrt((Ptm(1,i)-Ptm(1,j))**2+(Ptm(2,i)-Ptm(2,j))**2)
					end if
				end do
				do j=1,Nptf
					Aptmpart(:,i) = Aptmpart(:,i) + e*e*k/m*(Ptm(:,i+me*(Nptm/Np))-Ptf(:,j))/&
					&(sqrt((Ptm(1,i+me*(Nptm/Np))-Ptf(1,j))**2+(Ptm(2,i+me*(Nptm/Np))-Ptf(2,j))**2))**degp
				end do
				!viscosité
				Aptmpart(:,i) = Aptmpart(:,i) - nu*vptm(:,i+me*(Nptm/Np))*sqrt(vptm(1,i+me*(Nptm/Np))**2+vptm(2,i+me*(Nptm/Np))**2) 
				
				!confinement horizontal
				d1 = abs(Ptm(1,i+me*(Nptm/Np))-x(1,1))
				d2 = abs(Ptm(1,i+me*(Nptm/Np))-x(2,1))
				Aptmpart(1,i) = Aptmpart(1,i) + eb/Lx*e*k/m*(1./d1**degb-1./d2**degb)
				
				!confinement vertical
				d1 = abs(Ptm(2,i+me*(Nptm/Np))-x(1,2))
				d2 = abs(Ptm(2,i+me*(Nptm/Np))-x(2,2))
				Aptmpart(2,i) = Aptmpart(2,i) + eb/Ly*e*k/m*(1./d1**degb-1./d2**degb)
			end do	
			do  i=0,Np-1
				if (i/=me) then
					call mpi_send(Aptmpart,2*(Nptm/Np), MPI_DOUBLE_PRECISION,&
					&i,me, MPI_COMM_WORLD, statinfo)
				end if
			end do
			
		else
			allocate(Aptmpart(2,Nptm-me*Nptm/Np))
			
			do i = 1, Nptm-me*(Nptm/Np)
				do j= 1,Nptm
					if (j>i) then
						Aptmpart(:,i) = Aptmpart(:,i) + e*e*k/m*(Ptm(:,i+me*(Nptm/Np))-Ptm(:,j))/&
						&(sqrt((Ptm(1,i+me*(Nptm/Np))-Ptm(1,j))**2+(Ptm(2,i+me*(Nptm/Np))-Ptm(2,j))**2))**degp
						!Aptmpart(:,j) = Aptm(:,j) - e*e*k/m*(Ptm(:,i)-Ptm(:,j))/(sqrt((Ptm(1,i)-Ptm(1,j))**2+(Ptm(2,i)-Ptm(2,j))**2))**degp
						!print*,sqrt((Ptm(1,i)-Ptm(1,j))**2+(Ptm(2,i)-Ptm(2,j))**2)
					end if
				end do
				do j=1,Nptf
					Aptmpart(:,i) = Aptmpart(:,i) + e*e*k/m*(Ptm(:,i+me*(Nptm/Np))-Ptf(:,j))/&
					&sqrt((Ptm(1,i+me*(Nptm/Np))-Ptf(1,j))**2+(Ptm(2,i+me*(Nptm/Np))-Ptf(2,j))**2))**degp
				end do
				!viscosité
				Aptmpart(:,i) = Aptmpart(:,i) - nu*vptm(:,i)*sqrt(vptm(1,i)**2+vptm(2,i)**2) 
				
				!confinement horizontal
				d1 = abs(Ptm(1,i+me*(Nptm/Np))-x(1,1))
				d2 = abs(Ptm(1,i+me*(Nptm/Np))-x(2,1))
				Aptmpart(1,i) = Aptmpart(1,i) + eb/Lx*e*k/m*(1./d1**degb-1./d2**degb)
				
				!confinement vertical
				d1 = abs(Ptm(2,i+me*(Nptm/Np))-x(1,2))
				d2 = abs(Ptm(2,i+me*(Nptm/Np))-x(2,2))
				Aptmpart(2,i) = Aptmpart(2,i) + eb/Ly*e*k/m*(1./d1**degb-1./d2**degb)	
			end do
			
			do  i=0,Np-1
				if (i/=me) then
					call mpi_send(Aptmpart,2*(Nptm-me*Nptm/Np), MPI_DOUBLE_PRECISION, &
					&i,me, MPI_COMM_WORLD, statinfo)
				endif
			end do
			
		endif
		!print*, "avant", Aptm(1,:)
		do i=0,Np-1
			if (i<Np-1) then
				if (i/=me) then
					call mpi_recv(Aptm(:,i*Nptm/Np+1:(i+1)*Nptm/Np),2*(Nptm/Np),&
					&MPI_DOUBLE_PRECISION,i,i,MPI_COMM_WORLD,statu, statinfo)
				else
					Aptm(:,i*Nptm/Np+1:(i+1)*Nptm/Np)=Aptmpart
				end if
			else
				if (i/=me) then
				 call mpi_recv(Aptm(:,Nptm-(Np-1)*Nptm/Np:Nptm),2*(Nptm-(Np-1)*Nptm/Np),&
				 &MPI_DOUBLE_PRECISION,i,i,MPI_COMM_WORLD,statu, statinfo)
				else
					Aptm(:,i*Nptm/Np+1:(i+1)*Nptm/Np)=Aptmpart
				endif
			endif
		end do
		if (me==0) then
			print*, "apres",Aptm(1,:)
		endif
			
			print*, "comparaison", Aptmpart(1,:)
		
		deallocate(Aptmpart)
	end subroutine
	
	subroutine replacePt()
		implicit none
		
	!	do i = 1, Nptm
	!	print*, norme(Ptm(:,i))
	!		if (norme(Ptm(:,i)-centre)>rayon) then
	!			ptm(:,i) = centre + rayon*Ptm(:,i)/norme(Ptm(:,i))
	!		end if
	!	end do
		
		do i = 1, Nptm
			Ptm(1,i)=max(Ptm(1,i),x(1,1)+0.1)
			Ptm(1,i)=min(Ptm(1,i),x(2,1)-0.1)
			Ptm(2,i)=max(Ptm(2,i),x(1,2)+0.1)
	  	Ptm(2,i)=min(Ptm(2,i),x(2,2)-0.1)
		end do
		
	end subroutine
	
	function norme(v)
		implicit none
		real*8::norme,v(2)
		
		norme = sqrt(v(1)**2+v(2)**2)
		
	end function

end program
